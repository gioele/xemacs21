
(defvar debian-xemacs-major-version
  (number-to-string emacs-major-version)
  "XEMacs' Major Version as string")
(defvar debian-xemacs-version
  emacs-program-version
  "XEmacs' Full Version as string")

(setq gnus-nntpserver-file "/etc/news/server")

(setq mail-host-address (let ((name (expand-file-name "/etc/mailname")))
                          (if (not (file-readable-p name))
                              nil
                            (with-temp-buffer
                              (insert-file-contents-literally name)
                              (while (search-forward "\n" nil t)
                                (replace-match "" nil t))
                              (buffer-string)))))

(loop for entry in load-history
  if (string-match "^/home/.*xemacs21/xemacs\\(21\\)?-\\([.0-9]+/\\)"
                   (car entry))
  do (setf (car entry)
           (replace-match "/usr/share/xemacs-\\2" t nil (car entry))))

;;; From BTS #288807
(mwheel-install)
(setq mwheel-follow-mouse t)

;;; PSGML section
(setq-default
 sgml-local-ecat-files (quote ("ECAT" "~/sgml/ECAT"
                               "/usr/share/sgml/ECAT"
                               "/usr/local/share/sgml/ECAT"
                               "/usr/local/lib/sgml/ECAT"))
 sgml-local-catalogs (quote ("/etc/sgml/catalog"
                             "/usr/share/xemacs21/xemacs-packages/etc/psgml-dtds/CATALOG")))

(setq-default
 sgml-auto-activate-dtd t
 sgml-data-directory "/usr/share/sgml/declaration/"
 sgml-custom-markup '(("Version1" "<![%Version1[\r]]>")
                      ("New page"  "<?NewPage>"))
 sgml-xml-declaration "/usr/share/sgml/declaration/xml.dcl"
 sgml-public-map '("%S"  "/usr/share/sgml/%S" "/usr/share/sgml/%o/%c/%d"
		   "/usr/local/share/sgml/%o/%c/%d"
                   "/usr/local/lib/sgml/%o/%c/%d")
 sgml-set-face t
 sgml-system-path '("/usr/share/sgml"
                    "/usr/share/sgml/cdtd"
		    "/usr/local/share/sgml"
                    "/usr/local/lib/sgml")
 sgml-validate-command "onsgmls -s -m /etc/sgml/catalog %s %s ")

;;; END PSGML section

(setq module-load-path '("/usr/lib/xemacs/site-modules"))

(setq news-path "/var/spool/news")

(when (functionp 'make-coding-system)
    (make-coding-system 'us-ascii nil))

(global-set-key [iso-left-tab] [(shift tab)])
(global-set-key [(control iso-left-tab)] [(control shift tab)])

(defun dir-and-all-good-subs (this-directory)
  "Returns list of argument and all subdirectories of argument not
starting with a '.'"
  (if (file-exists-p this-directory)
      (append (list (expand-file-name this-directory))
              (mapcar '(lambda (dir-string)
                         (concat dir-string "/"))
                      (directory-files
                       (expand-file-name this-directory)
                       t "^[^\\.]" nil 1)))
    nil))

(setq load-path
      (append
       (dir-and-all-good-subs
        (expand-file-name "~/.xemacs/xemacs-packages"))
       (dir-and-all-good-subs
        (expand-file-name "~/.xemacs/packages"))
       (list (concat "/etc/xemacs" debian-xemacs-major-version))
       `(,@(dir-and-all-good-subs "/usr/local/lib/xemacs/site-lisp")
           ,@(dir-and-all-good-subs
              (concat "/usr/share/xemacs/site-lisp-"
                      debian-xemacs-major-version "/"))
           ,@(dir-and-all-good-subs "/usr/share/xemacs/site-lisp/")
           ,@(dir-and-all-good-subs
              (concat "/usr/share/xemacs" debian-xemacs-major-version
                      "/site-lisp/"))
           )
       load-path))

;;; end 00debian.el
